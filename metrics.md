## Objective

As at least a stage / section we need to make sure we have common shared terminology, definitions, and data for our metrics as they tend to get asked to be combined and compared.

## Plan

1. [Shared definitions](#shared-definitions) ✅
1. [Share and compare data methods](#share-and-compare-data-methods)
1. [Flatten data](#flatten-data)

### Shared definitions ✅

#### What is included

* Paid - User taking action within any paid tier namepace.
  * Q: Is this only when they are paying for the tier of the stage/group? i.e. for secure Ultimate/Gold?
    * A: No
  * Q: Should GitLab count as paid?
    * A: No
* MAU - unique users who have done any action within the section, stage, group, or category (depending on which chart it is). Includes GitLab team members.
  * For example - GMAU - SAST - If a user runs a (SAST scan AND a Secret scan) they count once, if a user runs a (SAST scan OR a Secret scan) they count once.
  * For example - SMAU - If a user runs a (SAST scan AND a Secret scan AND DAST scan AND Dependency Scan) they count once, if a user runs a (SAST scan OR a DAST scan) they count once.
  * Q: Do we WANT to not/include GitLab team members?
    * A: As users, yes, but we also want to easily be able to remove them when we are digging into things.

#### Known Caveats

We should write, and maintain, a list of known caveats of how the ideal is not being met, and our blockers for when it can be fixed.

##### Pageviews

* Pageviews today best we can do is use a cookie to track users - data team issue to track for when we'll get a unique key.
* Pageviews today - it’s a 3 point weight of work at least to record a click, which means we have to take into account cost/benefit when wanting to add click count, need to find data future plan to track for when this will improve if ever. This should be automatic with most web tracking interfaces.

##### GitLab filtration issues

* Issues with removing GitLab projects, namespaces and users
  * Today it is hard to remove GitLab team members, namespaces and projects. When will this get easier (data backlog item)? We desire to be able to remove them from PAID (because they don't) as well as remove them when digging into a finding (not MAU but research or exploratory).
* Issues with MAU
  * Today GitLab team members and projects are included in PMAU - 

##### Paid / tier issues

* Issues with determining "Paid"
  * Paid - today we know that trial users, gifted users (universities, OSS projects) are included in paid, data team is working on this
  * Paid - today GitLab team members and projects are included, but don't pay. Is there a longer term project to remove these?
  * Might be Nicole only - some tables don't record plan at time of event, and complex join to get that, these reports will assume today's plan was/is always the plan which is inaccurate (until we get to flatten step).
  * Payment is linked to namespace, a user can be in many namespaces, a user could be double counted when comparing Paid to un-paid or Tiers (but not for TMAU)

##### SaaS Issues

* Job tracking
  * for job tracking - look into changing to static key vs artifact name or job name - Is this resolved in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239118)? if yes all groups must replicate. Desire is to be able to not under or over report (users can change job name and artifact name), and to also be able to track failed jobs.
* SaaS
  * Waiting to progress past MVC on longer term improvements to data framework to better allow efficient web page action tracking.

##### Self-Hosted Isuues

* Self-Hosted
  * We are using MAX category for GMAU not actual GMAU so we are undercounting. Can not compute GMAU - we can do Category MAU and Category PMAU but because it's recorded on host it's a unique user per action recorded. We could add a new ping for each group, however as a group/stage changes this would become incorrect as you can not remove/add counts historically. For example - SCA was CS+LC+DS, now it's DS+lC and CS belongs to a new group. The numbers could not be compared historically month over month, in addition when comparing across self-hosted and SaaS to aggregate we would need to make srpoate charts for each date range to make sure we were comparing correctly - i.e. 2020 Jan-Sept = 3 categories compared to 3 categories and Oct + = 2 categories compared to 2 categories, ALSO we would need to exclude anyone on an older version as their ping would be sending wrong data.
* Is this resolved in [this line of code](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/ee/gitlab/usage_data.rb#L352)? Can not compute SMAU - we can do Category MAU and Category PMAU but because it's recorded on host it's a unique user per action recorded. We could add a new ping for each group stage, however if any categories move from one group to another it would cause issues. The numbers could not be compared historically month over month, in addition when comparing across self-hosted and SaaS to aggregate we would need to make srpoate charts for each date range to make sure we were comparing correctly - i.e. 2020 Jan-Sept = 3 categories compared to 3 categories and Oct + = 2 categories compared to 2 categories, ALSO we would need to exclude anyone on an older version as their ping would be sending wrong data, which then you might interpret them as "new" users as they upgrade. 
  * Inconsistent. Today Useage ping is not always sent by customers, leading to inconsistent number of instances and users reporting which could lead to a "dip" or fluctuation in numbers. Unsure how to resolve as it changes month by month unless extrapolation does month my month adjustment to compensate. Reference data backlog issue where they are going to be working to have a specific set of data to compare for MAU metrics excluding certain accounts to help with this from a MAU standpoint.
  * Statistically Significant? Today most self hosted can not, or will not send telemtry data. I worry using the data we have, extrapolated or not, it is not statistically significant enough to use as a metric. Data team backlog item to record all sold licenses so we know what % are reporting in. Data team item to record all unpaid items once they report in.
  * Extrapolation. Today most self hosted can not, or will not send telemtry data. We need to wait on `data team` to finish their project so that we can understand total available pool of self hosted and what % of data we are getting, and create a shared method to extrapolate usage consistently and fairly based on what we do have if it is statistically significant enough to do so. I worry it is not statistically significant enough to extrapolate.

##### Data Isuues

  * Data gets dropped / lost - we see very inconsistent data which hints at individual data being dropped not just in bulk.

##### Secure Engineering Isuues

* Secure Engineering issues
  * Improve accuracy of data being sent - for example SCA timeouts on send (deprioritized in many cases are working on other metrics items that could surperseed this data table)/ Second exmaple - wild pageview counts by orders of magnitude.
  * Improve accuracy of current flattening on data side (we use "like" which can over-catch some jobs and under-catch other jobs) 
* Shared consistent knowledge across Secure, and Product is lacking
  * DBT is not very robust as far as how some tables are built (circular references) or what data is (defined fields) or how best to connect the data to relevant infomation (suggested joins). 
  * Each PM is working independantly and self service, so there could be data disparity. 

### Share and compare data methods

Nicole to propose new flattening for secure and share with team, once approved will share with data.

Tables - [https://gitlab.com/gitlab-org/secure/general/-/blob/master/data.md](https://gitlab.com/gitlab-org/secure/general/-/blob/master/data.md)

We should also work to [add more information to the dbt docs](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#how-can-i-update-or-add-more-information-to-the-dbt-docs) when possible such as what options are available for a field and what they mean. [Link to DBT](https://dbt.gitlabdata.com/#!/overview).

### Flatten data

Work with data team to nightly? weekly? flatten the agreed upon tables and joins into a useable point in time table for our MAU based on their best practices.
