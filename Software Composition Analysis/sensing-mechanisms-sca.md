## Selecting items for the release

Our [Sensing Mechanisms](https://about.gitlab.com/handbook/product/product-processes/#sensing-mechanisms) and [Performance Indicators](https://about.gitlab.com/handbook/product/secure-and-defend-section-performance-indicators/) are included below in the details (expand to see).

### From Memory

 - Did I promise any customers anything? 
 - Any verbal trends from customer calls or from customer success? 
 - Anything new from analysts? 
 - Category Maturity goals? Currently ~"AST Leadership"
 - Product organization asks

### Recording Tools

- Chorus.ai
- Dovetail

### Metrics / Periscope

* [Secure Software Composition SCA Analysis Dashboard](https://app.periscopedata.com/app/gitlab/749790/Secure-Software-Composition-SCA-Analysis-Dashboard)
* [Secure SCA Dashboard - Performance](https://app.periscopedata.com/app/gitlab/764436/Secure-SCA-Dashboard---Performance)
* [Secure Metrics](https://app.periscopedata.com/app/gitlab/410654/Secure-Metrics)
* [Secure & Protect GMAU/SMAU Metrics](https://app.periscopedata.com/app/gitlab/707777/Secure-&-Protect-GMAU-SMAU-Metrics)
* [PI handbook page](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/#securecomposition-analysis---gmau---users-running-any-sca-scanners)

### Load Times

[product - performance - secure](https://dashboards.gitlab.net/d/product-secure/product-performance-secure?orgId=1)

### Competition

 - Competitor announcements I can counter?
    - [GitHub Roadmap](https://github.com/github/roadmap/projects/1)

### Documents

 - [Monthly CS Call](https://docs.google.com/document/d/1R2ATCJ5ephnt6NXWlRfK4be5DqXu0_ax7NcNBr8IoRg/edit#)

### Spreadsheets

- [Top ARR drivers for sales](https://docs.google.com/spreadsheets/d/1JdtaZYO90pR4_NQgSZRu9qdGuMrFj_H6qkBs5tFMeRc/edit#gid=0)

### Periscope

 - [User Requested Issues](https://app.periscopedata.com/app/gitlab/480786/User-Requested-Issues)
 - [Product Lost Deal Analysis](https://app.periscopedata.com/app/gitlab/432568/Product-Lost-Deal-Analysis)
 - [Product Management + Sales Opportunity Feedback](https://app.periscopedata.com/app/gitlab/705822/Product-Management-+-Sales-Opportunity-Feedback) related to handbook page [Command Plan](https://about.gitlab.com/handbook/sales/command-of-the-message/command-plan/) 

### PPS

* [handbook page](https://about.gitlab.com/direction/product-operations/#post-purchase)
* [Post Purchase Survey Q3 2020](https://docs.google.com/presentation/d/1NVb3ecscPhcQMC_wpznbXsDqKkK-09--6OhSj3bclXs/edit#slide=id.g59bfc474c5_2_145) - merge approvals are high, which makes me feel good about continuing to push forward UX polish with that as Matt can't right now. Dependency scanning and LC have dropped off the list.

### PNPS / SUS

* [handbook page](https://about.gitlab.com/direction/product-operations/#paid-nps)
* [periscope - gitlab.com Paid Net Promoter Score](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8521210&udv=0) (WIP)
* [slides January](https://docs.google.com/presentation/d/1o9oQc5YKqCxvoqorwDamlglddXgWV2C9Y9smD3cEXJM/edit#slide=id.g93170f0c7a_0_75)
* [PNPS: Bounce Rate and Response rate](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=9184725&udv=0) (WIP)
* [Q3 PNPS Responder Outreach](https://gitlab.com/gitlab-com/Product/-/issues/1766) - links to * [Q3 NPS Followups](https://docs.google.com/spreadsheets/d/1ToLUdy2K8tKO0mDGv9qhjBuXiNAzoQi558l85AnR43o/edit?ts=5fb82087#gid=0)

## no access

- https://docs.google.com/spreadsheets/d/1JXy4p8C9f9qvXLVisdSMoF_zKLfQhP6SKG3S1M5E9zk/edit#gid=0

### SUS

* [SUS score - PI](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/#system-usability-scale-sus-score)
* [Q3FY21 - System Usability Scale (First Look)](https://docs.google.com/spreadsheets/d/1pe99p0-sjcLMF1Ku6tzXOY-5UWLN7GWTnSi0fv6nwOU/edit#gid=38303263)

### Issues
 - Top [~"customer success"](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=customer%20success)
 - Top [~"customer"](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=customer)
 - Top [~"customer+"](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=customer%2B)
 - Top [~"internal customer"](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=internal%20customer)
