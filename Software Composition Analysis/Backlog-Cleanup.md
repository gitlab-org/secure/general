Handy shortcut
/epic &4488
/close 
This issue is not on the 12 month plan, no recent activity, but can reopen if interest picks back up. mention CA group PM to get attention https://about.gitlab.com/handbook/product/categories/#composition-analysis-group

## Epics

<details><summary>Click to expand...</summary>

### Epics without Devops::*

[epics](https://gitlab.com/groups/gitlab-org/-/epics?not%5Blabel_name%5D%5B%5D=devops%3A%3Aconfigure&not%5Blabel_name%5D%5B%5D=devops%3A%3Acreate&not%5Blabel_name%5D%5B%5D=devops%3A%3Adefend&not%5Blabel_name%5D%5B%5D=devops%3A%3Aenablement&not%5Blabel_name%5D%5B%5D=devops%3A%3Agrowth&not%5Blabel_name%5D%5B%5D=devops%3A%3Amanage&not%5Blabel_name%5D%5B%5D=devops%3A%3Amonitor&not%5Blabel_name%5D%5B%5D=devops%3A%3Apackage&not%5Blabel_name%5D%5B%5D=devops%3A%3Aplan&not%5Blabel_name%5D%5B%5D=devops%3A%3Arelease&not%5Blabel_name%5D%5B%5D=devops%3A%3Asecure&not%5Blabel_name%5D%5B%5D=devops%3A%3Averify&scope=all&state=opened)

[epics with extra weeding](https://gitlab.com/groups/gitlab-org/-/epics?not%5Blabel_name%5D%5B%5D=Category%3ABilling&not%5Blabel_name%5D%5B%5D=Category%3ACloud+Native+Installation&not%5Blabel_name%5D%5B%5D=Category%3AContinuous+Delivery&not%5Blabel_name%5D%5B%5D=Category%3ADependency+Proxy&not%5Blabel_name%5D%5B%5D=Category%3AGitLab+Handbook&not%5Blabel_name%5D%5B%5D=Category%3AIssue+Boards&not%5Blabel_name%5D%5B%5D=Category%3AKubernetes+Management&not%5Blabel_name%5D%5B%5D=Category%3AOmnibus+Package&not%5Blabel_name%5D%5B%5D=Category%3APages&not%5Blabel_name%5D%5B%5D=Category%3APipeline+Authoring&not%5Blabel_name%5D%5B%5D=Category%3ARelease+Orchestration&not%5Blabel_name%5D%5B%5D=Category%3ARoadmaps&not%5Blabel_name%5D%5B%5D=Category%3ASecrets+Management&not%5Blabel_name%5D%5B%5D=Category%3AStatic+Site+Editor&not%5Blabel_name%5D%5B%5D=Category%3AWeb+Performance&not%5Blabel_name%5D%5B%5D=Category%3AWiki&not%5Blabel_name%5D%5B%5D=GraphQL&not%5Blabel_name%5D%5B%5D=Manage+%5BDEPRECATED%5D&not%5Blabel_name%5D%5B%5D=Pajamas&not%5Blabel_name%5D%5B%5D=QA&not%5Blabel_name%5D%5B%5D=Quality&not%5Blabel_name%5D%5B%5D=Technical+Writing&not%5Blabel_name%5D%5B%5D=UX&not%5Blabel_name%5D%5B%5D=UX+problem+validation&not%5Blabel_name%5D%5B%5D=architecture+blueprint&not%5Blabel_name%5D%5B%5D=devops%3A%3Aconfigure&not%5Blabel_name%5D%5B%5D=devops%3A%3Acreate&not%5Blabel_name%5D%5B%5D=devops%3A%3Adefend&not%5Blabel_name%5D%5B%5D=devops%3A%3Aenablement&not%5Blabel_name%5D%5B%5D=devops%3A%3Agrowth&not%5Blabel_name%5D%5B%5D=devops%3A%3Amanage&not%5Blabel_name%5D%5B%5D=devops%3A%3Amonitor&not%5Blabel_name%5D%5B%5D=devops%3A%3Apackage&not%5Blabel_name%5D%5B%5D=devops%3A%3Aplan&not%5Blabel_name%5D%5B%5D=devops%3A%3Arelease&not%5Blabel_name%5D%5B%5D=devops%3A%3Asecure&not%5Blabel_name%5D%5B%5D=devops%3A%3Averify&not%5Blabel_name%5D%5B%5D=group%3A%3Aaccess&not%5Blabel_name%5D%5B%5D=group%3A%3Aacquisition&not%5Blabel_name%5D%5B%5D=group%3A%3Aanalytics&not%5Blabel_name%5D%5B%5D=group%3A%3Aapm&not%5Blabel_name%5D%5B%5D=group%3A%3Aconfigure&not%5Blabel_name%5D%5B%5D=group%3A%3Adatabase&not%5Blabel_name%5D%5B%5D=group%3A%3Adistribution&not%5Blabel_name%5D%5B%5D=group%3A%3Aecosystem&not%5Blabel_name%5D%5B%5D=group%3A%3Ageo&not%5Blabel_name%5D%5B%5D=group%3A%3Agitaly&not%5Blabel_name%5D%5B%5D=group%3A%3Agitter&not%5Blabel_name%5D%5B%5D=group%3A%3Aglobal+search&not%5Blabel_name%5D%5B%5D=group%3A%3Aimport&not%5Blabel_name%5D%5B%5D=group%3A%3Amemory&not%5Blabel_name%5D%5B%5D=group%3A%3Aportfolio+management&not%5Blabel_name%5D%5B%5D=group%3A%3Aproject+management&not%5Blabel_name%5D%5B%5D=group%3A%3Arelease+management&not%5Blabel_name%5D%5B%5D=group%3A%3Arunner&not%5Blabel_name%5D%5B%5D=group%3A%3Asource+code&not%5Blabel_name%5D%5B%5D=pajamas%3A%3Aintegrate&not%5Blabel_name%5D%5B%5D=webpack&page=14&scope=all&state=opened)

<details><summary>By Person</summary>
Olivier [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&author_username=gonzoyumo&not[label_name][]=devops%3A%3Asecure)

Philippe [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=plafoucriere)

Nicole [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=NicoleSchwartz)

Fabien [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=fcatteau)

Adam [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=adamcohen)

Igor [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=ifrenkel)

Kyle [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=kmann)

Mo [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=xlgmokha)

Tetiana [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=brytannia)

Neil [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=nmccorrison)

Ross [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=rossfuhrman)

Lucas [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=theoretick)

Zach [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=zrice)

Derek [Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=derekferguson)

Taylor [Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=tmccaslin)

Sam W [Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Adefend&author_username=sam.white)

Sam K [Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=stkerr&not[label_name][]=devops%3A%3Adefend)

Matt [Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&author_username=matt_wilson&not[label_name][]=devops%3A%3Adefend)

David [Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Adefend&author_username=david)

[Prior PMs](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&author_username=bikebilly&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Averify)
</details>

### Devops::Secure Epics without Group::*

[epics-without a group](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Asecure&not[label_name][]=all%20secure%20groups&not[label_name][]=group%3A%3Acomposition%20analysis&not[label_name][]=group%3A%3Adynamic%20analysis&not[label_name][]=group%3A%3Afuzz%20testing&not[label_name][]=group%3A%3Astatic%20analysis&not[label_name][]=group%3A%3Athreat%20insights&not[label_name][]=group%3A%3Avulnerability%20research&not[label_name][]=QA&not[label_name][]=no%20secure%20group&not[label_name][]=group%3A%3Acontainer%20security)

[All secure groups-label](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=all%20secure%20groups)

[No group-label](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=no%20secure%20group)

### Devops::Secure Group::Composition Analysis Epics without a Category:*

[epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Asecure&not[label_name][]=Category%3AContainer%20Scanning&not[label_name][]=Category%3ADependency%20Scanning&not[label_name][]=Category%3ALicense%20Compliance&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=all%20secure%20groups)

</details>

## Issues

### Issues without Devops::*

<details><summary>Click to expand...</summary>

[board](https://gitlab.com/groups/gitlab-org/-/boards/1134668)

smaller project scope [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?not%5Blabel_name%5D%5B%5D=Category%3ANavigation&not%5Blabel_name%5D%5B%5D=Category%3AUser+Management&not%5Blabel_name%5D%5B%5D=Create+%5BDEPRECATED%5D&not%5Blabel_name%5D%5B%5D=Quality&not%5Blabel_name%5D%5B%5D=devops%3A%3Aconfigure&not%5Blabel_name%5D%5B%5D=devops%3A%3Acreate&not%5Blabel_name%5D%5B%5D=devops%3A%3Adefend&not%5Blabel_name%5D%5B%5D=devops%3A%3Aenablement&not%5Blabel_name%5D%5B%5D=devops%3A%3Agrowth&not%5Blabel_name%5D%5B%5D=devops%3A%3Amanage&not%5Blabel_name%5D%5B%5D=devops%3A%3Amonitor&not%5Blabel_name%5D%5B%5D=devops%3A%3Apackage&not%5Blabel_name%5D%5B%5D=devops%3A%3Aplan&not%5Blabel_name%5D%5B%5D=devops%3A%3Arelease&not%5Blabel_name%5D%5B%5D=devops%3A%3Asecure&not%5Blabel_name%5D%5B%5D=devops%3A%3Averify&not%5Blabel_name%5D%5B%5D=group%3A%3Acompliance&not%5Blabel_name%5D%5B%5D=group%3A%3Aconfigure&not%5Blabel_name%5D%5B%5D=group%3A%3Adistribution&not%5Blabel_name%5D%5B%5D=group%3A%3Aglobal+search&not%5Blabel_name%5D%5B%5D=group%3A%3Aprogressive+delivery&not%5Blabel_name%5D%5B%5D=triage+report&page=276&scope=all&state=opened)

</details>

### Issues without Devops::* by person

<details><summary>by person</summary>

- [Nicole](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=NicoleSchwartz)

- [Olivier](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=gonzoyumo)

- [Philippe](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=plafoucriere)

- [Fabien](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=fcatteau)

- [Adam](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=adamcohen)

- [Igor](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=ifrenkel)

- [Kyle](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=kmann)

- [Mo](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=xlgmokha)

- [Tetiana](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=brytannia)

- [Neil](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=nmccorrison)

- [Ross](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=rossfuhrman)

- [Lucas](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=theoretick)

- [Zach](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=zrice)

- [Derek](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=derekferguson)

- [Taylor](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=tmccaslin)

- [Sam W](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=sam.white)

- [Sam K](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=stkerr)

- [Matt](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=matt_wilson)

- [David](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=david)

- [Prior PM](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Category%3ANavigation&not[label_name][]=Category%3AUser%20Management&not[label_name][]=Create%20%5BDEPRECATED%5D&not[label_name][]=Quality&not[label_name][]=devops%3A%3Aconfigure&not[label_name][]=devops%3A%3Acreate&not[label_name][]=devops%3A%3Adefend&not[label_name][]=devops%3A%3Aenablement&not[label_name][]=devops%3A%3Agrowth&not[label_name][]=devops%3A%3Amanage&not[label_name][]=devops%3A%3Amonitor&not[label_name][]=devops%3A%3Apackage&not[label_name][]=devops%3A%3Aplan&not[label_name][]=devops%3A%3Arelease&not[label_name][]=devops%3A%3Asecure&not[label_name][]=devops%3A%3Averify&not[label_name][]=group%3A%3Acompliance&not[label_name][]=group%3A%3Aconfigure&not[label_name][]=group%3A%3Adistribution&not[label_name][]=group%3A%3Aglobal%20search&not[label_name][]=group%3A%3Aprogressive%20delivery&not[label_name][]=triage%20report&author_username=bikebilly)

</details>

## Secure Issues

### Issues Devops::Secure No Group::*

[board](https://gitlab.com/groups/gitlab-org/-/boards/1394529?&label_name[]=devops%3A%3Asecure)

### Issues Devops::Secure No Category:*

[board](https://gitlab.com/groups/gitlab-org/-/boards/1273679?&label_name[]=devops%3A%3Asecure)

## SCA Issues

<details><summary>Click to expand...</summary>

### Group::Composition Analysis No Category:*

[board](https://gitlab.com/groups/gitlab-org/-/boards/1273679?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Acomposition%20analysis)

[issue list](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=Category%3ADependency%20Scanning&not[label_name][]=Category%3AContainer%20Scanning&not[label_name][]=Category%3ALicense%20Compliance&not[label_name][]=meta&not[assignee_username][]=willmeek&not[label_name][]=all%20secure%20groups)

### Group::Composition Analysis No Devops::secure

[issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=devops%3A%3Asecure)

### Group::Composition Analysis No GitLab Ultimate
[Missing ~"GitLab Ultimate](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=GitLab%20Ultimate)

### Group::Composition Analysis No Enterprise Edition
[Missing ~"Enterprise Edition"](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=Enterprise%20Edition)

### Missing Type

[About Type Labels](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#type-labels)

~feature
~bug
~tooling
~meta
~documentation [check for missing types](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=Planning%20Issue&not[label_name][]=backstage&not[label_name][]=bug&not[label_name][]=documentation&not[label_name][]=feature&not[label_name][]=feature%3A%3Aaddition&not[label_name][]=feature%3A%3Amaintenance&not[label_name][]=tooling&not[label_name][]=tooling%3A%3Apipelines&not[label_name][]=tooling%3A%3Aworkflow&not[label_name][]=triage%20report&not[label_name][]=QA&not[label_name][]=Product%20Management&not[label_name][]=technical%20debt&not[label_name][]=pajamas%3A%3Aintegrate&not[label_name][]=meta&not[label_name][]=UX%20Research%20request)

### Assigned to Frontend or Backend
 - Not ~frontend or ~backend or assigned to QA or Tech Writing [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=backend&not[label_name][]=frontend&not[assignee_username][]=willmeek&not[assignee_username][]=ngaskill&not[label_name][]=meta&not[label_name][]=UX%20Research%20request&not[assignee_username][]=kmann&not[assignee_username][]=NicoleSchwartz)

### No Epic
 - No epic [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&epic_id=None&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=bug)

### In Closed Milestones

 - Not in upcoming milestone [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[milestone_title]=%23upcoming)

### No Milestone

 - No milestone [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[milestone_title]=%23upcoming)

### Should have ~"Accepting merge requests"

~"Accepting merge requests" [label not present](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=Accepting%20merge%20requests&not[label_name][]=meta)

</details>

## Milestone Cleanup

<details><summary>Click to Expand</summary>

No ~Deliverable no ~Stretch [board](https://gitlab.com/groups/gitlab-org/-/boards/1134726?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&not[label_name][]=workflow%3A%3Aplanning%20breakdown&not[label_name][]=workflow%3A%3Aproblem%20validation&not[label_name][]=workflow%3A%3Arefinement&not[label_name][]=workflow%3A%3Adesign&milestone_title=13.7&not[assignee_username]=willmeek&not[label_name][]=meta)

</details>
