General Links  
  * [Composition Analysis group Categories](https://about.gitlab.com/handbook/product/categories/#composition-analysis-group)
  * [How we work - Secure](https://about.gitlab.com/handbook/engineering/development/secure/)   
  * CA Slack channel: [#g_secure-composition-analysis](https://gitlab.slack.com/archives/CKWHYU7U2)
  * [Secure Direction](https://about.gitlab.com/direction/secure/)
  * [Direction - Dependency Scanning](https://about.gitlab.com/direction/secure/composition-analysis/dependency-scanning/)
  * [Docs - Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
  * [Direction - License Compliance](https://about.gitlab.com/direction/secure/composition-analysis/license-compliance/)
  * [Docs - License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/#license-compliance)
  * [Secure Performance Indicators](https://about.gitlab.com/handbook/product/secure-and-defend-section-performance-indicators/)
  * [Secure Performance Indicators - Internal Handbook](https://internal-handbook.gitlab.io/product/performance-indicators/sec-section/#securecomposition-analysis---gmau---users-running-any-sca-scanners)

FAQ
  * [Vulnerability Scanner Maintenance](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#vulnerability-scanner-maintenance)
  * [Vulnerability Databse Sources](https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/master/SOURCES.md)

Priorities
  * [CA priorities for the year 2022](https://gitlab.com/gitlab-org/secure/general/-/issues/187)
  * [15.0 Deprecations and Removals](https://gitlab.com/groups/gitlab-org/-/epics/6157#note_597208976)
  * [16.0 deprecations and removals](https://gitlab.com/groups/gitlab-org/-/epics/7391)]

Boards
  * [CA Bug Board](https://gitlab.com/groups/gitlab-org/-/boards/1077546?scope=all&utf8=%E2%9C%93&label_name[]=bug&label_name[]=devops%3A%3Asecure&not[label_name][]=group%3A%3Afuzz%20testing&not[label_name][]=group%3A%3Astatic%20analysis&not[label_name][]=group%3A%3Avulnerability%20research&not[label_name][]=group%3A%3Athreat%20insights&not[label_name][]=group%3A%3Adynamic%20analysis)

Older Links
  * [CA priorities for the year 2021](https://gitlab.com/gitlab-org/secure/general/-/issues/134)
