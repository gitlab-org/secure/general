### Selecting items for the release

Our [Sensing Mechanisms](https://about.gitlab.com/handbook/product/product-processes/#sensing-mechanisms) and [Performance Indicators](https://about.gitlab.com/handbook/product/secure-and-defend-section-performance-indicators/) are included below in the details.

[SCA sensing mechanisms](https://gitlab.com/gitlab-org/secure/general/-/blob/master/Software%20Composition%20Analysis/sensing-mechanisms-sca.md)

Performance/Quality
- [infradev SCA](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=infradev&label_name[]=group::composition+analysis)
- [infradev Secure](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=infradev&label_name[]=devops%3A%3Asecure)
- [security P1](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=security&label_name[]=priority::1)
- [security P2](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=security&label_name[]=priority::2)
- [security P3](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=security&label_name[]=priority::3)
- [security P4](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=security&label_name[]=priority::4)
- [security other](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=security&not[label_name][]=priority::1&not[label_name][]=priority::2&not[label_name][]=priority::3&not[label_name][]=priority::4))
- [P1 bug](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=priority::1&label_name[]=type::bug&not[label_name][]=security)
- [P2 bug](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=priority::2&label_name[]=type::bug&not[label_name][]=security)
- [P3 bug](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=priority::3&label_name[]=type::bug&not[label_name][]=security)
- [P4 bug](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=priority::4&label_name[]=type::bug&not[label_name][]=security)
- [bug other](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::composition+analysis&label_name[]=type::bug&not[label_name][]=security&not[label_name][]=priority::1&not[label_name][]=priority::2&not[label_name][]=priority::3&not[label_name][]=priority::4)
- ~performance [issue](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=performance)
- ~reliability [issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=reliability)
- ~availability [issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=availability)

Useability/SUS
- [SUS FY22 Q1 - Incomplete](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=SUS%3A%3AFY22%20Q1%20-%20Incomplete)
- [Q1 secure](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=SUS%3A%3AFY22%20Q1%20-%20Incomplete&label_name[]=devops%3A%3Asecure)
- [SUS FY22 Q2 - Incomplete](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=SUS%3A%3AFY22%20Q2%20-%20Incomplete)
- [Q2 secure](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=SUS%3A%3AFY22%20Q2%20-%20Incomplete&label_name[]=devops%3A%3Asecure)
- ~"UI polish" [UI Polish issue(s)](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=UI%20polish)
- ~"UX debt" [UX Debt issue(s)](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=UX%20debt)

Quality
- ~test [issue(s)](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=test)

Maintiance
- ~"technical debt" [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=technical%20debt)
- ~"backstage [DEPRECATED]"  [issue(s)](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=backstage&label_name[]=group%3A%3Acomposition%20analysis)
- ~"tooling" [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acomposition%20analysis&label_name[]=tooling)

Customer
- [~customer](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&label_name[]=customer)
- [~customer success](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&label_name[]=customer+success)
- [~customer+](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&label_name[]=customer+)
- [~internal customer](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&label_name[]=internal+customer)

Backlog
- [Backlog](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&milestone_title=Backlog)
- [Backlog not bug or sec](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&milestone_title=Backlog&not[label_name][]=type::bug&not[label_name][]=security)
- [Backlog P1](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&label_name[]=priority::1&milestone_title=Backlog&not[label_name][]=type::bug&not[label_name][]=security)
- [Next 1-3](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&milestone_title=Next+1-3+releases)
- [Next 3-4](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&milestone_title=Next+3-4+releases)
- [Next 4-7](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&milestone_title=Next+4-7+releases)
- [Next 7-10](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&milestone_title=Next+7-13+releases)
- [Awaiting furthur demand](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=priority_desc&state=opened&label_name[]=group::composition+analysis&milestone_title=Awaiting+further+demand)

[backlog cleanup](https://gitlab.com/gitlab-org/secure/general/-/blob/master/Software%20Composition%20Analysis/Backlog-Cleanup.md)
