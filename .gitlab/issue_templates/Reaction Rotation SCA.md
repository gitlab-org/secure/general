# Composition Analysis - Reaction Rotation [MILESTONE]

## Problem to solve

Track and document activities performed during the [Reaction Rotation](https://about.gitlab.com/handbook/engineering/development/sec/secure/composition-analysis/#reaction-rotation).

## DRI

* Primary engineer: [@primary-engineer]
* Secondary engineer: [@secondary-engineer]

## Activity Threads

Please create a thread for each of the following sections.
During the rotation, add the relevant updates under each thread.
Focus on significant findings, patterns, and actionable insights rather than routine alerts.

---

### 1. 🤝 Handover [PREVIOUS_MILESTONE] → [MILESTONE] (Internal)

Maintain continuity between rotations by transferring knowledge and context about ongoing tasks
* [ ] Create an internal [thread](link-to-thread) (as it might contain security updates)
* [ ] Ask the previous rotation's primary engineer to do a handover. You can use this template:

```markdown
@previous-primary-engineer
Could you please share any ongoing tasks, important context, or pending items from your rotation
that I should be aware of?
```

---

### 2. 🔐 Security (Internal)

Security related topics.
* [ ] Create an internal [thread](link-to-thread)

---

### 3. 📞 Support

Support related topics
(For example, [Requests for Help](https://gitlab.com/gitlab-com/request-for-help)).
* [ ] Create [thread](link-to-thread)

---

### 4. 🔧 Maintainership

Maintainership related topics.
For example, which docker images have been updated
* [ ] Create a [thread](link-to-thread)

---

### 4. 📝 Continuous Feedback

Record observations and suggestions throughout the rotation period:
* What worked well?
* What could be improved?
* Bottlenecks identified
* Process improvement suggestions

* [ ] Create a [thread](link-to-thread)

---

## Reference

* [Previous Rotation Reports](https://gitlab.com/gitlab-org/secure/general/-/issues/?sort=created_date&state=all&in=TITLE&search=Composition%20Analysis%20-%20Reaction%20Rotation)
* [Rotation Process Documentation](https://about.gitlab.com/handbook/engineering/development/sec/secure/composition-analysis/#reaction-rotation)

/label ~"section::sec" ~"devops::application security testing" ~"group::composition analysis" ~"type::ignore"
