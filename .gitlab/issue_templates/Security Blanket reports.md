# Process

Each week, groups in Application Security Testing are expected to provide updates on [roadmap initiatives](https://docs.google.com/presentation/d/1ABoGLJkQZNs3Y92NELNrRvjsbo_PNEjGMyCRVz2sU2A/edit?usp=sharing) which are underway. Updates can be delegated as needed. Please add/remove projects as necessary.

Updates are to be added as new threads on this issue.

## Meta

- Issue title format: `YYYY-MM-DD — Application Security Testing weekly updates`
- Assignee: Whomever is responsible for ensuring updates have been submitted for each group. (Default: @twoodham)

### Weekly update format

```
# Topic: <group-name /> - <name />

- point: [Project name](epic url) - % complete, health status, launch date/milestone, 1 sentence summary
- point: [Project name](epic url) - % complete, health status, launch date/milestone, 1 sentence summary
- point: [Project name](epic url) - % complete, health status, launch date/milestone, 1 sentence summary
```

### Roadmap status updates

We have status meeting updates each Tuesday at 7 a.m. PST. As a stage, we are responsible for updating the corresponding breakout slide with the content generated in this weekly issue. @twoodham will be verifying content is updated in both locations prior to the end of his week.

### Closing comment

```
@maw, @sarahwaldner, @hbenson - :waves:

FYI on this week's report from engineering teams in Application Security Testing. Summary can be found at <summary-section-url />
```

## Checklist

- [ ] @thiagocsf
- [ ] @amarpatel
- [ ] @tkopel
- [ ] @dabeles
- [ ] @twoodham

/label ~"section::sec" ~"devops::application security testing"  ~"type::ignore" ~discoto ~"security blanket"
/assign @twoodham
/epic https://gitlab.com/groups/gitlab-org/secure/-/epics/5
/confidential
