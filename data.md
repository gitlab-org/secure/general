# Table of Contents

- [Resources and Documentation](resources-and-documentation)
- [Data Tables and Queries](data-tables-and-queries)
- [Joins](#joins)
- [Questions](#questions)

## Resources and Documentation

### Documentation
- [Sisense For Cloud Data Teams‎](https://about.gitlab.com/handbook/business-ops/data-team/platform/periscope/#self-serve-analysis-in-periscope)
- dbt [Data Documentation for GitLab](https://dbt.gitlabdata.com/#!/overview)
- [Data For Product Managers](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/)
- Contributor and Development Docs [Telemetry Guide](https://docs.gitlab.com/ee/development/telemetry/index.html)
- [ERD diagram](https://drive.google.com/file/d/1AUNikGqgBNvpkt7VlXI6cPfMTxdkAqmv/view)

### Website
- [Product Direction - Telemetry](https://about.gitlab.com/direction/product-analytics/)

### Handbook
- [Product Team Performance Indicators](https://about.gitlab.com/handbook/product/performance-indicators/)
- [Secure and Defend Section Performance Indicators](https://about.gitlab.com/handbook/product/secure-and-defend-section-performance-indicators/)
- [Product Processes - Product Metrics](https://about.gitlab.com/handbook/product/product-processes/#product-metrics)
- [Product Processes - Telemetry at GitLab](https://about.gitlab.com/handbook/product/product-processes/#telemetry-at-gitlab)
- [Snowplow events snippets - Use these SQL snippets to help you get started visualizing your Snowplow events.](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#snowplow-events-snippets)
- [Some Issues and Merge Requests examples](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#some-issues-and-merge-requests-examples)
- [Self-Service Data](https://about.gitlab.com/handbook/business-ops/data-team/direction/self-service/)
- [Product Geolocation Analysis](https://about.gitlab.com/handbook/business-ops/data-team/data-catalog/product-geolocation/)
- [Customer Segmentation](https://about.gitlab.com/handbook/business-ops/data-team/data-catalog/customer-segmentation/)


### Issue Template

- [Snowplow event tracking](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Snowplow%20event%20tracking)

### Periscope 

#### Topics
-[Secure Stage](https://app.periscopedata.com/app/gitlab/topic/Secure-Stage/abd1f99b8f2d4259945de8a58928cb7f)

#### Dashboards
- [Data - Customer Segmentation](https://app.periscopedata.com/app/gitlab/718514/Customer-Segmentation)
- [Secure GMAU & SMAU Metrics](https://app.periscopedata.com/app/gitlab/707777/Secure-GMAU-&-SMAU-Metrics)
- [Secure Metrics](https://app.periscopedata.com/app/gitlab/410654/Secure-Metrics)
- [Secure Software Composition Analysis Dashboard](https://app.periscopedata.com/app/gitlab/749790/Secure-Software-Composition-Analysis-Dashboard)
- [Snowploe PI examples](https://app.periscopedata.com/app/gitlab/752659/Snowplow-PI-Examples)

## Proposal

use analytics_staging.gitlab_dotcom_ci_JOB_artifacts exact name match to decide what type of job it is?

join that against namespaces to determine paid, and users to determine userid /gitlab team member

join against ?? to find out current paid state that day

values needed?
- date
- status (sucess, fail)
- artifact size
- run time
- type (category)
- group
- stage
- user
- paid
- tier
- trial
- gitlab?
- yaml
- if child pipeline
- branch?
- retried?

## Data Tables and Queries

Jump to - 

- [Analytics tables](#analytics)
- [Analytics Staging Tables](#analytics_staging) (not the engineering concept)


### analytics

Jump to Table - 

- [analytics.gitlab_dotcom_ci_builds](#analyticsgitlab_dotcom_ci_builds)
- [analytics.gitlab_dotcom_secure_stage_ci_jobs](#analyticsgitlab_dotcom_secure_stage_ci_jobs)
- [analytics.gitlab_dotcom_usage_data_events](#analyticsgitlab_dotcom_usage_data_events)
- [analytics.version_usage_data_month](#analyticsversion_usage_data_month)
- [analytics.GITLAB_DOTCOM_PROJECTS_XF](#analyticsgitlab_dotcom_projects_xf)
- [analytics.GITLAB_DOTCOM_USERS_XF](#analyticsgitlab_dotcom_users_xf)
- [analytics.GITLAB_DOTCOM_NAMESPACES_XF](#analyticsgitlab_dotcom_nasmespaces_xf)
- [analytics.snowplow_page_views_all](#analyticssnowplow_page_views_all)

#### analytics.GITLAB_DOTCOM_NAMESPACES_XF

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.gitlab_dotcom_namespaces_xf

Make update to source: https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/staging/gitlab_dotcom/xf/schema.yml

Notes: Payment (tier) is by namespace (which is Group or Individual) not instance / user. - not an easy way to include or exclude all GitLab namespaces today buut being worked by data team.

#### analytics.gitlab_dotcom_users_xf

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.gitlab_dotcom_users_xf

Make update to source: https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/staging/gitlab_dotcom/xf/schema.yml

Notes: Generally used today to remove / call out GitLab team members (approximation - TBD data team working to make better way to include or exclude GitLab team members)

Query: Find GitLab employee user accounts on saas 

```
Select 
user_id, 
public_email, --would there be non dot-com employee email addresses?
notification_email, --would there be non dot-com employee email addresses?
organization -- do we require or set this for GitLab employees or is it optional/freeform
From analytics.gitlab_dotcom_users_xf
where 
organization like '%GitLab%' or 
public_email like '%GitLab.com' or notification_email like '%GitLab.com' 
```

#### analytics.GITLAB_DOTCOM_PROJECTS_XF

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.gitlab_dotcom_projects_xf

Make update to source: https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/staging/gitlab_dotcom/xf/schema.yml

Notes: Nicole uses to join mostly - this table has the namespace tier

Join: 
```
from analytics.gitlab_dotcom_secure_stage_ci_jobs as jobs
inner join analytics.GITLAB_DOTCOM_PROJECTS_XF as projects
on jobs.ci_build_project_id = projects.project_id
```

#### analytics.gitlab_dotcom_ci_builds  

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.gitlab_dotcom_ci_builds

Make update to source:

Notes: All builds not just our flattened (which seems to be missing data so may be more reliable)

Join: join on projects to get tier

```
from analytics.gitlab_dotcom_ci_builds as builds
inner join analytics.GITLAB_DOTCOM_PROJECTS_XF as projects
on builds.ci_build_project_id = projects.project_id
```

#### analytics.gitlab_dotcom_usage_data_events

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.gitlab_dotcom_usage_data_events

Make update to source: https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/staging/gitlab_dotcom/xf/schema.yml

<details><summary>Query:</summary>

```
select 
user_id, 
event_created_at, 
event_name, 
stage_name, 
plan_name_at_event_date, 
plan_id_at_event_date  
from analytics.gitlab_dotcom_usage_data_events
where stage_name = 'secure'
```

Query to find if new secure stuff added: 

```
Select 
namespace_id, 
user_id, 
parent_type, --project/group but seems to always be project for secure
parent_id,
is_representative_of_stage, --true/false
event_name, --sast, 
stage_name, --%secure%
plan_name_at_event_date
FROM analytics.gitlab_dotcom_usage_data_events
where stage_name like '%secure%'
and event_name != 'container_scanning'
and event_name != 'sast'
and event_name != 'dependency_scanning'
and event_name != 'dast'
and event_name != 'secret_detection'
and event_name != 'secure_stage_ci_jobs'
and event_name != 'license_management'
and event_name != 'license_scanning'
```

</details>

#### analytics.version_usage_data_month

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.version_usage_data_month

Make update to source: 

<details><summary>Query:</summary>

Query: Useage Ping data

```
select 
date_trunc('month',created_at::date), 
ping_source, 
ping_id, 
company, 
instance_user_count, 
edition, 
main_edition, 
edition_type, 
projects, CI_PIPELINE_CONFIG_AUTO_DEVOPS, auto_devops_enabled, dependency_list_usages_total, container_scanning_jobs, license_management_jobs,  secret_detection_jobs, dast_jobs, dependency_scanning_jobs, sast_jobs, coverage_fuzzing_jobs
from analytics.version_usage_data_month
where created_at>='2020-01-01' and 
(dependency_list_usages_total is not null OR 
 container_scanning_jobs is not null OR 
 license_management_jobs is not null OR 
 secret_detection_jobs is not null OR 
 dast_jobs is not null OR 
 dependency_scanning_jobs is not null OR 
 sast_jobs is not null OR 
 coverage_fuzzing_jobs is not null)
group by 1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
```

</details>

#### analytics.gitlab_dotcom_secure_stage_ci_jobs

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.gitlab_dotcom_secure_stage_ci_jobs

Make update to source: https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/staging/gitlab_dotcom/xf/schema.yml

<details><summary>Query:</summary>

Query: Distinct count of projects and jobs ran, for each artifact type, seems to have more fields than analytics_staging.gitlab_dotcom_ci_JOB_artifacts but not sure if different sources?

```
select secure_ci_job_type, Count(distinct(ci_build_id)), count(distinct(ci_build_project_id)) 
from analytics.gitlab_dotcom_secure_stage_ci_jobs
group by 1
order by 1
```
Information about statuses of runs

```
select status, secure_ci_job_type, Count(distinct(ci_build_id)), count(distinct(ci_build_project_id))
from analytics.gitlab_dotcom_secure_stage_ci_jobs
group by 1,2
order by 1,2
```

Table contains: ci_build_id, status, created_at, finished_at, ci_build_name, ci_build_project_id, ARTIFACTS_SIZE, retried, failure_reason, secure_ci_job_type, and more...

Field `secure_ci_job_type` seems normalized as compared to `ci_build_name`

Field `ARTIFACTS_SIZE` seems to always be empty

Field `retried` seems to be true or false

Field `failure_reason` is a number

Suggest Joins: gitlab_dotcom_projects_xf, GITLAB_DOTCOM_NAMESPACES_XF

```
Select 
jobs.ci_build_id, 
jobs.created_at, 
jobs.ci_build_runner_id, 
jobs.ci_build_name, 
jobs.SECURE_CI_JOB_TYPE, 
jobs.ci_build_user_id, 
jobs.ci_build_project_id,
projects.project_name, 
projects.namespace_plan_title, 
projects.NAMESPACE_PLAN_IS_PAID,
projects.member_count, 
projects.namespace_id, 
namespace.namespace_id, 
namespace.namespace_name, 
namespace.namespace_type, 
namespace.parent_id, 
namespace.plan_title, 
namespace.plan_is_paid, 
namespace.owner_id, 
namespace.member_count, 
namespace.project_count, 
namespace.namespace_ultimate_parent_id 
from analytics.gitlab_dotcom_secure_stage_ci_jobs as jobs
left join analytics.gitlab_dotcom_projects_xf as projects
on jobs.ci_build_project_id = projects.project_id 
left join analytics.GITLAB_DOTCOM_NAMESPACES_XF as namespace
on projects.namespace_id = namespace.namespace_id
```
</details>

#### analytics.snowplow_page_views_all 

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.snowplow_page_views_all 

Make update to source: https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/staging/snowplow/combined/all/schema.yml

Notes: USER_SNOWPLOW_DOMAIN_ID FROM analytics.snowplow_page_views_all 

Q: is that session, or unique user id (perminant) aka is that valid for MAU/GMAU

A:  not valid for MAU/GMAU IMO. It's a temporary cookie

See row 50 in this file https://docs.google.com/spreadsheets/d/1okmqh53TFNZs3V166QS9oA930-UeV5APCJZB5G9Zg_o/edit#gid=1478985823

Here's my thoughts on Manage SMAU https://gitlab.com/gitlab-data/analytics/-/issues/6669#note_433970699

### analytics_staging

Jump to table - 

- [analytics_staging.gitlab_dotcom_ci_JOB_artifacts](#analytics_staginggitlab_dotcom_ci_JOB_artifacts)

#### analytics_staging.gitlab_dotcom_ci_JOB_artifacts

Source: https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.gitlab_dotcom_ci_job_artifacts

Make update to source:

Notes: Potentially the most reliable way to ID secure jobs in SaaS?

<details><summary>Query:</summary>

Query: Distinct count of projects and jobs ran, for each artifact type

```
select 
Case 
 when file = 'gl-license-management-report.json' then 'license_scanning'
 when file = 'gl-license-scanning-report.json' then 'license_scanning'
 when file = 'gl-container-scanning-report.json' then 'container_scanning'
 when file = 'gl-coverage-fuzzing.json' then 'fuzzing'
 when file = 'gl-dast-report.json' then 'dast'
 when file = 'gl-dependency-scanning-report.json' then 'dependency_scanning'
 when file = 'gl-sast-report.json' then 'sast'
 when file = 'gl-secret-detection-report.json' then 'secret_detection'
 else '' end as file, 
count(ci_job_id), count(DISTINCT(project_id))
from analytics_staging.gitlab_dotcom_ci_JOB_artifacts
where file like '%.json' AND file != 'browser-performance.json' and file != 'gl-accessibility.json' and file != 'gl-code-quality-report.json' and file != 'load-performance.json' and file != 'lsif.json'  
and file != 'metrics_referee.json' 
and file != 'performance.json' 
and file != 'requirements.json' 
and file != 'tfplan.json' 
group by 1
order by 1
```

</details>

## Joins

### Link job to paid or not

Concern: doesn't take into account paid AT THAT date

```
select 
  date_trunc('month',jobs.created_at::date) as jobmonth, 
  count(distinct(ci_build_user_id)) as MAU 
from analytics.gitlab_dotcom_secure_stage_ci_jobs as jobs
  Left Join analytics.GITLAB_DOTCOM_PROJECTS_XF as projects
  on
  jobs.ci_build_project_id = 
  projects.PROJECT_ID
where jobs.created_at>='2020-01-01' AND jobs.created_at<='2020-12-31' AND  jobs.SECURE_CI_JOB_TYPE in ('dependency_scanning', 'container_scanning', 'license_scanning') and projects.NAMESPACE_PLAN_IS_PAID = 'TRUE'
group by 1
order by 1
```

## Questions

Q: analytics.GITLAB_DOTCOM_NAMESPACES_XF.namespace_type options; NULL, Individual, Group - What exactly is a namespace? 

A: It's Individual (https://gitlab.com/NicoleSchwartz) or Group (https://gitlab.com/gitlab-org)

Q: So what is a NULL namespace mean?

A: null is result of left join finding no matching data. Missing data (do inner join to exclude misssing data) unknown why data is missing.

Q: why is analytics.gitlab_dotcom_ci_builds.ARTIFACTS_FILE always null, is this data anywhere? I want it as I assume I could then parse this to figure out what the job was?

A: a new table got created gitlab_dotcom_ci_job_artifacts, where you should join secure_stage_ci_jobs on and you have the size column here

```
seLECT job.ci_build_id, size
FROM analytics.gitlab_dotcom_secure_stage_ci_jobs AS job
LEFT JOIN analytics_staging.gitlab_dotcom_ci_job_artifacts AS art ON job.ci_build_id = art.ci_job_id 
```

Q: Why did artifact size stop being populated in June 2019 in analytics.gitlab_dotcom_secure_stage_ci_jobs? https://gitlab.com/gitlab-data/analytics/-/issues/6474

<details><summary>Query</summary>

```
select 
--count(distinct(ci_build_id)) as builds, 
--ci_build_id, 
--status, 
--created_at,
date_trunc('month',created_at::date) as month, 
SECURE_CI_JOB_TYPE ,
AVG(ALL ARTIFACTS_SIZE) 
from analytics.gitlab_dotcom_secure_stage_ci_jobs 
--where 
--ARTIFACTS_SIZE is not null and 
--ARTIFACTS_SIZE != '0.0' and
--created_at>='2020-01-01' AND created_at<='2020-12-31' 
group by 1,2
order by 1,2
```

</details>

A: a new table got created gitlab_dotcom_ci_job_artifacts, where you should join secure_stage_ci_jobs on and you have the size column here

```
seLECT job.ci_build_id, size
FROM analytics.gitlab_dotcom_secure_stage_ci_jobs AS job
LEFT JOIN analytics_staging.gitlab_dotcom_ci_job_artifacts AS art ON job.ci_build_id = art.ci_job_id 
``` 

Q: I am attempting to monitor the average size (and max size) of artifacts - the only table i can find is a staging table, is there a way to find this data for production? From a statistical point of view is this staging good enough to do perforamnce and risk monitoring as well as to correlate as potentially influencial on production customer actions? (i.e. increased disatisfaction)

<details><summary>Query</summary>

```
Select 
date_trunc('month',created_at::date) as month, 
case when file = 'gl-dast-report.json' then 'dast'
when file = 'gl-dependency-scanning-report.json' then 'dependency-scanning'
when file = 'gl-container-scanning-report.json' then 'container-scanning'
when file = 'gl-license-management-report.json' then 'license-compliance'
when file = 'gl-license-scanning-report.json' then 'license-compliance'
when file like '%fuzz%' then 'fuzzing'
when file = 'gl-sast-report.json' then 'sast'
when file like '%secret%' then 'secret-detection'
else '' end as category, 
avg(ALL size) as average_size 
from analytics_staging.gitlab_dotcom_ci_JOB_artifacts
where file like '%.json' and
file != 'gl-code-quality-report.json' and
file != 'performance.json' and
file != 'gl-accessibility.json' and
file != 'browser-performance.json' and 
file != 'tfplan.json' and 
file != 'lsif.json' and 
file != 'requirements.json' and 
file != 'metrics_referee.json' and 
file != 'browser-performance.json' and 
file != 'load-performance.json' and
created_at>='2020-01-01' AND created_at<='2020-12-31' 
group by 1,2
order by 1,2
```

</details>

A: ["These are two different schemas inside of our analytics data warehouse.
analytics_staging does NOT have anything to do with the software engineering concept of a staging environment."](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#whats-the-difference-between-analytics_staging-and-analytics)
