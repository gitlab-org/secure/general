# General

All discussions about the organization of the teams within the Secure stage.
Issues opened in this project should result in process changes or (preferably)
updates to [our company handbook section](https://about.gitlab.com/handbook/engineering/development/secure/).

Any discussions related to development of the GitLab product must happen within
the [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab) issue tracker.


Other specific places:
 - retrospective project: https://gitlab.com/gl-retrospectives/secure
 - release process: https://gitlab.com/gitlab-org/security-products/release
